/**
 * Copyright 2015 Cenote GmbH.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.cenote.oformsci.anttask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.ExecuteStreamHandler;
import org.apache.tools.ant.taskdefs.ExecuteWatchdog;
import org.apache.tools.ant.taskdefs.Redirector;
import org.apache.tools.ant.taskdefs.condition.Os;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.Environment;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Mapper;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.RedirectorElement;
import org.apache.tools.ant.types.Resource;
import org.apache.tools.ant.types.ResourceCollection;
import org.apache.tools.ant.types.ResourceFactory;
import org.apache.tools.ant.types.resources.FileProvider;
import org.apache.tools.ant.types.resources.FileResource;
import org.apache.tools.ant.util.CompositeMapper;
import org.apache.tools.ant.util.FileNameMapper;
import org.apache.tools.ant.util.FileUtils;
import org.apache.tools.ant.util.FlatFileNameMapper;
import org.apache.tools.ant.util.GlobPatternMapper;
import org.apache.tools.ant.util.IdentityMapper;
import org.apache.tools.ant.util.LinkedHashtable;
import org.apache.tools.ant.util.ResourceUtils;
import org.apache.tools.ant.util.SourceFileScanner;

import de.cenote.oformsci.AppInfo;

/**
 * @author volker.vosskaemper@cenote.de
 * 
 *         This class is based on org/apache/tools/ant/taskdefs/ExecTask.java
 *         org/apache/tools/ant/taskdefs/Copy.java
 * 
 */
public class Formsc extends Task {
	private static final String MSG_WHEN_COPYING_EMPTY_RC_TO_FILE = "Cannot perform operation from directory to file.";

	static final File NULL_FILE_PLACEHOLDER = new File("/NULL_FILE");
	static final String LINE_SEPARATOR = System.getProperty("line.separator");
	protected File file = null; // the source file
	protected File destFile = null; // the destination file
	protected File destDir = null; // the destination directory
	protected Vector<ResourceCollection> rcs = new Vector<ResourceCollection>();
	// here to provide API backwards compatibility
	protected Vector<ResourceCollection> filesets = rcs;

	private boolean enableMultipleMappings = false;
	protected boolean filtering = false;
	protected boolean preserveLastModified = false;
	protected boolean autoFullbuild = true;
	protected boolean forceOverwrite = false;
	protected boolean flatten = false;
	protected int verbosity = Project.MSG_VERBOSE;
	protected boolean failonerror = true;
	private boolean failimmediate = false;
	private boolean compiledWithError = false;
	private List<String> filesWithError = new ArrayList<String>();

	protected Hashtable<String, String[]> fileCopyMap = new LinkedHashtable<String, String[]>();
	protected Hashtable<String, String[]> dirCopyMap = new LinkedHashtable<String, String[]>();
	protected Hashtable<File, File> completeDirMap = new LinkedHashtable<File, File>();

	protected Mapper mapperElement = null;
	protected FileUtils fileUtils;
	private long granularity = 0;
	private boolean quiet = false;

	private static final FileUtils FILE_UTILS = FileUtils.getFileUtils();

	private String os;
	private String osFamily;

	private File dir;
	protected boolean newEnvironment = false;
	private Long timeout = null;
	private Environment env = new Environment();
	protected Commandline formsCompilerCmdline = new Commandline();
	protected Commandline reportsCompilerCmdline = new Commandline();
	private String resultProperty;
	private boolean failIfExecFails = true;
	private String formsCompiler;
	private String reportsCompiler;
	private boolean resolveExecutable = false;
	private boolean searchPath = false;
	private boolean spawn = false;
	private boolean incompatibleWithSpawn = false;

	// include locally for screening purposes
	private String inputString;
	private File input;
	private File output;
	private File error;

	protected Redirector redirector = new Redirector(this);
	protected RedirectorElement redirectorElement;

	private String dbuserid;

	/**
	 * Controls whether the VM (1.3 and above) is used to execute the command
	 */
	private boolean vmLauncher = true;

	/**
	 * Forms Compile task constructor.
	 */
	public Formsc() {
		fileUtils = FileUtils.getFileUtils();
		granularity = fileUtils.getFileTimestampGranularity();
	}

	/**
	 * create an instance that is helping another task. Project, OwningTarget,
	 * TaskName and description are all pulled out
	 * 
	 * @param owner
	 *            task that we belong to
	 */
	public Formsc(Task owner) {
		bindToOwner(owner);
	}

	/**
	 * Get the FileUtils for this task.
	 * 
	 * @return the fileutils object.
	 */
	protected FileUtils getFileUtils() {
		return fileUtils;
	}

	// /**
	// * Set the destination directory.
	// *
	// * @param destDir
	// * the destination directory.
	// */
	// public void setTodir(final File destDir) {
	// this.destDir = destDir;
	// }

	/**
	 * Set auto fullbuild mode
	 * 
	 * @param autoFullbuild
	 *            if true force overwriting of destination file(s) even if the
	 *            destination file(s) are younger than the corresponding source
	 *            file if an modified pll file is detected. Default is true.
	 */
	public void setAutoFullbuild(final boolean autoFullbuild) {
		this.autoFullbuild = autoFullbuild;
	}

	/**
	 * Set overwrite mode regarding existing destination file(s).
	 * 
	 * @param overwrite
	 *            if true force overwriting of destination file(s) even if the
	 *            destination file(s) are younger than the corresponding source
	 *            file. Default is false.
	 */
	public void setOverwrite(final boolean overwrite) {
		this.forceOverwrite = overwrite;
	}

	/**
	 * Set whether files copied from directory trees will be "flattened" into a
	 * single directory. If there are multiple files with the same name in the
	 * source directory tree, only the first file will be copied into the
	 * "flattened" directory, unless the forceoverwrite attribute is true.
	 * 
	 * @param flatten
	 *            if true flatten the destination directory. Default is false.
	 */
	public void setFlatten(final boolean flatten) {
		this.flatten = flatten;
	}

	/**
	 * Set verbose mode.
	 * 
	 * @param verbose
	 *            whether to output the full compile output.
	 */
	public void setVerbose(final boolean verbose) {
		this.verbosity = verbose ? Project.MSG_INFO : Project.MSG_VERBOSE;
	}

	/**
	 * Is verbose mode set.
	 * 
	 * @return true or false.
	 */
	public boolean isVerbose() {
		if (this.verbosity != Project.MSG_VERBOSE) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Set quiet mode. Used to hide messages when a file or directory does not
	 * exist.
	 *
	 * @param quiet
	 *            whether or not to display error messages when a file or
	 *            directory does not exist. Default is false.
	 */
	public void setQuiet(final boolean quiet) {
		this.quiet = quiet;
	}

	/**
	 * Set method of handling mappers that return multiple mappings for a given
	 * source path.
	 * 
	 * @param enableMultipleMappings
	 *            If true the task will copy to all the mappings for a given
	 *            source path, if false, only the first file or directory is
	 *            processed. By default, this setting is false to provide
	 *            backward compatibility with earlier releases.
	 * @since Ant 1.6
	 */
	public void setEnableMultipleMappings(final boolean enableMultipleMappings) {
		this.enableMultipleMappings = enableMultipleMappings;
	}

	/**
	 * Get whether multiple mapping is enabled.
	 * 
	 * @return true if multiple mapping is enabled; false otherwise.
	 */
	public boolean isEnableMultipleMapping() {
		return enableMultipleMappings;
	}

	/**
	 * Add a set of files to copy.
	 * 
	 * @param set
	 *            a set of files to copy.
	 */
	public void addFileset(final FileSet set) {
		add(set);
	}

	/**
	 * Add a collection of files to copy.
	 * 
	 * @param res
	 *            a resource collection to copy.
	 * @since Ant 1.7
	 */
	public void add(final ResourceCollection res) {
		rcs.add(res);
	}

	/**
	 * Define the mapper to map source to destination files.
	 * 
	 * @return a mapper to be configured.
	 * @exception BuildException
	 *                if more than one mapper is defined.
	 */
	private Mapper createMapper() throws BuildException {
		if (mapperElement != null) {
			throw new BuildException("Cannot define more than one mapper", getLocation());
		}
		mapperElement = new Mapper(getProject());
		return mapperElement;
	}

	/**
	 * Add a nested filenamemapper.
	 * 
	 * @param fileNameMapper
	 *            the mapper to add.
	 * @since Ant 1.6.3
	 */
	private void add(final FileNameMapper fileNameMapper) {
		createMapper().add(fileNameMapper);
	}

	/**
	 * Set the number of milliseconds leeway to give before deciding a target is
	 * out of date.
	 *
	 * <p>
	 * Default is 1 second, or 2 seconds on DOS systems.
	 * </p>
	 * 
	 * @param granularity
	 *            the granularity used to decide if a target is out of date.
	 * @since Ant 1.6.2
	 */
	public void setGranularity(final long granularity) {
		this.granularity = granularity;
	}

	public void setDbuserid(final String dbuserid) {
		this.dbuserid = dbuserid;
	}

	/**
	 * Perform the compile operation.
	 * 
	 * @exception BuildException
	 *                if an error occurs.
	 */
	@Override
	public void execute() throws BuildException {

		log(AppInfo.getProductVersion());

		final File savedFile = file; // may be altered in validateAttributes
		final File savedDestFile = destFile;
		final File savedDestDir = destDir;
		ResourceCollection savedRc = null;
		if (file == null && destFile != null && rcs.size() == 1) {
			// will be removed in validateAttributes
			savedRc = rcs.elementAt(0);
		}

		try {
			// make sure we don't have an illegal set of options
			try {
				validateAttributes();
			} catch (final BuildException e) {
				if (failonerror || !getMessage(e).equals(MSG_WHEN_COPYING_EMPTY_RC_TO_FILE)) {
					throw e;
				} else {
					log("Warning: " + getMessage(e), Project.MSG_ERR);
					return;
				}
			}

			// we create our own FileNameMapper here
			// saving the from and to patterns as key/value pairs in a hashmap
			Map<String, String> patterns = new HashMap<String, String>();
			patterns.put("*.pll", "*.plx");
			patterns.put("*.fmb", "*.fmx");
			patterns.put("*.mmb", "*.mmx");
			patterns.put("*.rdf", "*.rep");

			// nested Mappers must be contained in a CompositeMapper
			CompositeMapper compositeMapper = new CompositeMapper();
			for (Map.Entry<String, String> pattern : patterns.entrySet()) {
				GlobPatternMapper mapper = new GlobPatternMapper();
				mapper.setFrom(pattern.getKey());
				mapper.setTo(pattern.getValue());
				mapper.setCaseSensitive(false);
				compositeMapper.add(mapper);
			}
			add(compositeMapper);

			// deal with the single file
			copySingleFile();

			/*
			 * for historical and performance reasons we have to do things in a
			 * rather complex way.
			 * 
			 * (1) Move is optimized to move directories if a fileset has been
			 * included completely, therefore FileSets need a special treatment.
			 * This is also required to support the failOnError semantice (skip
			 * filesets with broken basedir but handle the remaining
			 * collections).
			 * 
			 * (2) We carry around a few protected methods that work on basedirs
			 * and arrays of names. To optimize stuff, all resources with the
			 * same basedir get collected in separate lists and then each list
			 * is handled in one go.
			 */

			final HashMap<File, List<String>> filesByBasedir = new HashMap<File, List<String>>();
			final HashMap<File, List<String>> dirsByBasedir = new HashMap<File, List<String>>();
			final HashSet<File> baseDirs = new HashSet<File>();
			final ArrayList<Resource> nonFileResources = new ArrayList<Resource>();
			final int size = rcs.size();
			for (int i = 0; i < size; i++) {
				final ResourceCollection rc = rcs.elementAt(i);

				if (rc instanceof FileSet && rc.isFilesystemOnly()) {
					final FileSet fs = (FileSet) rc;
					DirectoryScanner ds = null;
					try {
						ds = fs.getDirectoryScanner(getProject());
					} catch (final BuildException e) {
						if (failonerror || !getMessage(e).endsWith(DirectoryScanner.DOES_NOT_EXIST_POSTFIX)) {
							throw e;
						} else {
							if (!quiet) {
								log("Warning: " + getMessage(e), Project.MSG_ERR);
							}
							continue;
						}
					}
					final File fromDir = fs.getDir(getProject());

					final String[] srcFiles = ds.getIncludedFiles();
					final String[] srcDirs = ds.getIncludedDirectories();
					if (!flatten && mapperElement == null && ds.isEverythingIncluded() && !fs.hasPatterns()) {
						completeDirMap.put(fromDir, destDir);
					}
					add(fromDir, srcFiles, filesByBasedir);
					add(fromDir, srcDirs, dirsByBasedir);
					baseDirs.add(fromDir);
				} else { // not a fileset or contains non-file resources

					if (!rc.isFilesystemOnly() && !supportsNonFileResources()) {
						throw new BuildException("Only FileSystem resources are supported.");
					}

					for (final Resource r : rc) {
						if (!r.isExists()) {
							final String message = "Warning: Could not find resource " + r.toLongString() + " to copy.";
							if (!failonerror) {
								if (!quiet) {
									log(message, Project.MSG_ERR);
								}
							} else {
								throw new BuildException(message);
							}
							continue;
						}

						File baseDir = NULL_FILE_PLACEHOLDER;
						String name = r.getName();
						final FileProvider fp = r.as(FileProvider.class);
						if (fp != null) {
							final FileResource fr = ResourceUtils.asFileResource(fp);
							baseDir = getKeyFile(fr.getBaseDir());
							if (fr.getBaseDir() == null) {
								name = fr.getFile().getAbsolutePath();
							}
						}

						// copying of dirs is trivial and can be done
						// for non-file resources as well as for real
						// files.
						if (r.isDirectory() || fp != null) {
							add(baseDir, name, r.isDirectory() ? dirsByBasedir : filesByBasedir);
							baseDirs.add(baseDir);
						} else { // a not-directory file resource
							// needs special treatment
							nonFileResources.add(r);
						}
					}
				}
			}

			iterateOverBaseDirs(baseDirs, dirsByBasedir, filesByBasedir);
			// iterateOverBaseDirs is called twice because it can set
			// forceOverwrite=true in case of autoFullbuild=true
			iterateOverBaseDirs(baseDirs, dirsByBasedir, filesByBasedir);

			// do all the copy operations now...
			try {
				doFileOperations();
			} catch (final BuildException e) {
				if (!failonerror) {
					if (!quiet) {
						log("Warning: " + getMessage(e), Project.MSG_ERR);
					}
				} else {
					throw e;
				}
			}

		} finally {
			// clean up again, so this instance can be used a second
			// time

			file = savedFile;
			destFile = savedDestFile;
			destDir = savedDestDir;
			if (savedRc != null) {
				rcs.insertElementAt(savedRc, 0);
			}
			fileCopyMap.clear();
			dirCopyMap.clear();
			completeDirMap.clear();
		}
	}

	/************************************************************************
	 ** protected and private methods
	 ************************************************************************/

	private void copySingleFile() {
		// deal with the single file
		if (file != null) {
			if (file.exists()) {
				if (destFile == null) {
					destFile = new File(destDir, file.getName());
				}
				if (forceOverwrite || !destFile.exists()
						|| (file.lastModified() - granularity > destFile.lastModified())) {
					fileCopyMap.put(file.getAbsolutePath(), new String[] { destFile.getAbsolutePath() });
				} else {
					log(file + " omitted as " + destFile + " is up to date.", Project.MSG_VERBOSE);
				}
			} else {
				final String message = "Warning: Could not find file " + file.getAbsolutePath() + " to copy.";
				if (!failonerror) {
					if (!quiet) {
						log(message, Project.MSG_ERR);
					}
				} else {
					throw new BuildException(message);
				}
			}
		}
	}

	private void iterateOverBaseDirs(final HashSet<File> baseDirs, final HashMap<File, List<String>> dirsByBasedir,
			final HashMap<File, List<String>> filesByBasedir) {

		for (final File f : baseDirs) {
			final List<String> files = filesByBasedir.get(f);
			final List<String> dirs = dirsByBasedir.get(f);

			String[] srcFiles = new String[0];
			if (files != null) {
				srcFiles = files.toArray(srcFiles);
			}
			String[] srcDirs = new String[0];
			if (dirs != null) {
				srcDirs = dirs.toArray(srcDirs);
			}
			scan(f == NULL_FILE_PLACEHOLDER ? null : f, destDir, srcFiles, srcDirs);
		}
	}

	/**
	 * Ensure we have a consistent and legal set of attributes, and set any
	 * internal flags necessary based on different combinations of attributes.
	 * 
	 * @exception BuildException
	 *                if an error occurs.
	 */
	protected void validateAttributes() throws BuildException {
		if (file == null && rcs.size() == 0) {
			throw new BuildException("Specify at least one source--a fileset or a resource collection.");
		}
		final ResourceCollection rcTest = rcs.elementAt(0);
		if (!rcTest.isFilesystemOnly()) {
			throw new BuildException("Only FileSystem resources are supported.");
		}
		if (destFile != null && destDir != null) {
			throw new BuildException("Only one of tofile and todir may be set.");
		}
		if (destFile == null && destDir == null) {
			// Set destDir to the dir of the first fileset.
			if (rcTest instanceof FileSet) {
				final FileSet fs = (FileSet) rcTest;
				final File fromDir = fs.getDir(getProject());
				destDir = fromDir;
			} else {
				throw new BuildException("Todir must be set.");
			}
		}
		if (file != null && file.isDirectory()) {
			throw new BuildException("Use a resource collection to copy directories.");
		}
		if (destFile != null && rcs.size() > 0) {
			if (rcs.size() > 1) {
				throw new BuildException("Cannot concatenate multiple files into a single file.");
			} else {
				final ResourceCollection rc = rcs.elementAt(0);
				if (!rc.isFilesystemOnly() && !supportsNonFileResources()) {
					throw new BuildException("Only FileSystem resources are" + " supported.");
				}
				if (rc.size() == 0) {
					throw new BuildException(MSG_WHEN_COPYING_EMPTY_RC_TO_FILE);
				} else if (rc.size() == 1) {
					final Resource res = rc.iterator().next();
					final FileProvider r = res.as(FileProvider.class);
					if (file == null) {
						if (r != null) {
							file = r.getFile();
						} else {
							// singleResource = res;
						}
						rcs.removeElementAt(0);
					} else {
						throw new BuildException("Cannot concatenate multiple files into a single file.");
					}
				} else {
					throw new BuildException("Cannot concatenate multiple files into a single file.");
				}
			}
		}
		if (destFile != null) {
			destDir = destFile.getParentFile();
		}
		if (dbuserid == null) {
			throw new BuildException("dbuserid must be specified.");
		}
		// now check the compiler configuration
		checkConfiguration();
	}

	/**
	 * Compares source files to destination files to see if they should be
	 * copied.
	 *
	 * @param fromDir
	 *            The source directory.
	 * @param toDir
	 *            The destination directory.
	 * @param files
	 *            A list of files to copy.
	 * @param dirs
	 *            A list of directories to copy.
	 */
	protected void scan(final File fromDir, final File toDir, final String[] files, final String[] dirs) {
		final FileNameMapper mapper = getMapper();
		buildMap(fromDir, toDir, files, mapper, fileCopyMap);
	}

	/**
	 * Compares source resources to destination files to see if they should be
	 * copied.
	 *
	 * @param fromResources
	 *            The source resources.
	 * @param toDir
	 *            The destination directory.
	 *
	 * @return a Map with the out-of-date resources as keys and an array of
	 *         target file names as values.
	 *
	 * @since Ant 1.7
	 */
	protected Map<Resource, String[]> scan(final Resource[] fromResources, final File toDir) {
		return buildMap(fromResources, toDir, getMapper());
	}

	/**
	 * Add to a map of files/directories to copy.
	 *
	 * @param fromDir
	 *            the source directory.
	 * @param toDir
	 *            the destination directory.
	 * @param names
	 *            a list of filenames.
	 * @param mapper
	 *            a <code>FileNameMapper</code> value.
	 * @param map
	 *            a map of source file to array of destination files.
	 */
	protected void buildMap(final File fromDir, final File toDir, final String[] names, final FileNameMapper mapper,
			final Hashtable<String, String[]> map) {
		String[] toCheck = null;
		String[] toCopy = null;

		// check if there are modified pll files
		final SourceFileScanner dsCheck = new SourceFileScanner(this);
		toCheck = dsCheck.restrict(names, fromDir, toDir, mapper, granularity);
		for (int i = 0; i < toCheck.length; i++) {
			if (toCheck[i].toLowerCase().endsWith(".pll") && autoFullbuild && !forceOverwrite) {
				log("Changed pll file detected: " + toCheck[i] + " force full build");
				// iterateOverBaseDirs is called twice so on the next run
				// a full fileset is created
				setOverwrite(true);
			}
		}

		if (forceOverwrite) {
			final Vector<String> v = new Vector<String>();
			for (int i = 0; i < names.length; i++) {
				if (mapper.mapFileName(names[i]) != null) {
					v.addElement(names[i]);
				}
			}
			toCopy = new String[v.size()];
			v.copyInto(toCopy);
		} else {
			final SourceFileScanner ds = new SourceFileScanner(this);
			toCopy = ds.restrict(names, fromDir, toDir, mapper, granularity);
		}
		for (int i = 0; i < toCopy.length; i++) {
			final File src = new File(fromDir, toCopy[i]);
			final String[] mappedFiles = mapper.mapFileName(toCopy[i]);

			if (!enableMultipleMappings) {
				map.put(src.getAbsolutePath(), new String[] { new File(toDir, mappedFiles[0]).getAbsolutePath() });
			} else {
				// reuse the array created by the mapper
				for (int k = 0; k < mappedFiles.length; k++) {
					mappedFiles[k] = new File(toDir, mappedFiles[k]).getAbsolutePath();
				}
				map.put(src.getAbsolutePath(), mappedFiles);
			}
		}
	}

	/**
	 * Create a map of resources to copy.
	 *
	 * @param fromResources
	 *            The source resources.
	 * @param toDir
	 *            the destination directory.
	 * @param mapper
	 *            a <code>FileNameMapper</code> value.
	 * @return a map of source resource to array of destination files.
	 * @since Ant 1.7
	 */
	protected Map<Resource, String[]> buildMap(final Resource[] fromResources, final File toDir,
			final FileNameMapper mapper) {
		final HashMap<Resource, String[]> map = new HashMap<Resource, String[]>();
		Resource[] toCopy = null;
		if (forceOverwrite) {
			final Vector<Resource> v = new Vector<Resource>();
			for (int i = 0; i < fromResources.length; i++) {
				if (mapper.mapFileName(fromResources[i].getName()) != null) {
					v.addElement(fromResources[i]);
				}
			}
			toCopy = new Resource[v.size()];
			v.copyInto(toCopy);
		} else {
			toCopy = ResourceUtils.selectOutOfDateSources(this, fromResources, mapper, new ResourceFactory() {
				public Resource getResource(final String name) {
					return new FileResource(toDir, name);
				}
			}, granularity);
		}
		for (int i = 0; i < toCopy.length; i++) {
			final String[] mappedFiles = mapper.mapFileName(toCopy[i].getName());
			for (int j = 0; j < mappedFiles.length; j++) {
				if (mappedFiles[j] == null) {
					throw new BuildException(
							"Can't copy a resource without a" + " name if the mapper doesn't" + " provide one.");
				}
			}

			if (!enableMultipleMappings) {
				map.put(toCopy[i], new String[] { new File(toDir, mappedFiles[0]).getAbsolutePath() });
			} else {
				// reuse the array created by the mapper
				for (int k = 0; k < mappedFiles.length; k++) {
					mappedFiles[k] = new File(toDir, mappedFiles[k]).getAbsolutePath();
				}
				map.put(toCopy[i], mappedFiles);
			}
		}
		return map;
	}

	/**
	 * Actually does the file (and possibly empty directory) copies. This is a
	 * good method for subclasses to override.
	 */
	protected void doFileOperations() {
		log("verbose=" + isVerbose());
		log("overwrite=" + forceOverwrite, verbosity);
		if (fileCopyMap.size() > 0) {
			log("using environment:", verbosity);
			String[] envvars = env.getVariables();
			for (String var : envvars) {
				log("    " + var, verbosity);
			}

			log("");
			log("compiling " + fileCopyMap.size() + " file" + (fileCopyMap.size() == 1 ? "" : "s") + " to "
					+ destDir.getAbsolutePath());
			log("using forms compiler  : " + formsCompiler);
			log("using reports compiler: " + reportsCompiler);
			log("");

			// We take care on compile order here by saving all from/to pairs
			// into separate TreeMaps for each file type.
			// Then compile plls first.

			Map<String, String> plls = new TreeMap<String, String>();
			Map<String, String> mmbs = new TreeMap<String, String>();
			Map<String, String> fmbs = new TreeMap<String, String>();
			Map<String, String> rdfs = new TreeMap<String, String>();

			for (final Map.Entry<String, String[]> e : fileCopyMap.entrySet()) {
				final String fromFile = e.getKey();
				final String[] toFiles = e.getValue();

				for (int i = 0; i < toFiles.length; i++) {
					final String toFile = toFiles[i];

					if (fromFile.equals(toFile)) {
						log("Skipping self-copy of " + fromFile, verbosity);
						continue;
					}

					// store each from/to pair in its separate HashMap
					if (fromFile.toLowerCase().endsWith(".pll")) {
						plls.put(fromFile, toFile);
					}
					if (fromFile.toLowerCase().endsWith(".mmb")) {
						mmbs.put(fromFile, toFile);
					}
					if (fromFile.toLowerCase().endsWith(".fmb")) {
						fmbs.put(fromFile, toFile);
					}
					if (fromFile.toLowerCase().endsWith(".rdf")) {
						rdfs.put(fromFile, toFile);
					}
				}
			}

			// now loop over the maps for each type
			// all plls
			for (Map.Entry<String, String> entry : plls.entrySet()) {
				log("compiling " + entry.getKey());
				executeCompiler(new File(entry.getKey()), new File(entry.getValue()));
			}
			// all mmbs
			for (Map.Entry<String, String> entry : mmbs.entrySet()) {
				log("compiling " + entry.getKey());
				executeCompiler(new File(entry.getKey()), new File(entry.getValue()));
			}
			// all fmbs
			for (Map.Entry<String, String> entry : fmbs.entrySet()) {
				log("compiling " + entry.getKey());
				executeCompiler(new File(entry.getKey()), new File(entry.getValue()));
			}
			// all rdfs
			for (Map.Entry<String, String> entry : rdfs.entrySet()) {
				log("compiling " + entry.getKey());
				executeCompiler(new File(entry.getKey()), new File(entry.getValue()));
			}

			// if failimmediate=false check if an error occured and fail the
			// build
			if (!failimmediate && compiledWithError) {
				log("");
				log("-------------------------------------------------------------------");
				log("");
				log("The following files compiled with error:");
				log("");
				for (String sourceFile : filesWithError) {
					log(sourceFile);
				}
				log("");
				log(filesWithError.size() + " files with error.");
				throw new BuildException(getTaskType() + " " + filesWithError.size() + " file(s) compiled with error",
						getLocation());
			}
		} else {
			log("Nothing to compile! All files are up to date in " + destDir + ".");
		}

	}

	/**
	 * Whether this task can deal with non-file resources.
	 */
	protected boolean supportsNonFileResources() {
		return false;
	}

	/**
	 * Adds the given strings to a list contained in the given map. The file is
	 * the key into the map.
	 */
	private static void add(File baseDir, final String[] names, final Map<File, List<String>> m) {
		if (names != null) {
			baseDir = getKeyFile(baseDir);
			List<String> l = m.get(baseDir);
			if (l == null) {
				l = new ArrayList<String>(names.length);
				m.put(baseDir, l);
			}
			l.addAll(java.util.Arrays.asList(names));
		}
	}

	/**
	 * Adds the given string to a list contained in the given map. The file is
	 * the key into the map.
	 */
	private static void add(final File baseDir, final String name, final Map<File, List<String>> m) {
		if (name != null) {
			add(baseDir, new String[] { name }, m);
		}
	}

	/**
	 * Either returns its argument or a plaeholder if the argument is null.
	 */
	private static File getKeyFile(final File f) {
		return f == null ? NULL_FILE_PLACEHOLDER : f;
	}

	/**
	 * returns the mapper to use based on nested elements or the flatten
	 * attribute.
	 */
	private FileNameMapper getMapper() {
		FileNameMapper mapper = null;
		if (mapperElement != null) {
			mapper = mapperElement.getImplementation();
		} else if (flatten) {
			mapper = new FlatFileNameMapper();
		} else {
			mapper = new IdentityMapper();
		}
		return mapper;
	}

	/**
	 * Handle getMessage() for exceptions.
	 * 
	 * @param ex
	 *            the exception to handle
	 * @return ex.getMessage() if ex.getMessage() is not null otherwise return
	 *         ex.toString()
	 */
	private String getMessage(final Exception ex) {
		return ex.getMessage() == null ? ex.toString() : ex.getMessage();
	}

	// /**
	// * Returns a reason for failure based on the exception thrown. If the
	// * exception is not IOException output the class name, output the message
	// if
	// * the exception is MalformedInput add a little note.
	// */
	// private String getDueTo(final Exception ex) {
	// final boolean baseIOException = ex.getClass() == IOException.class;
	// final StringBuffer message = new StringBuffer();
	// if (!baseIOException || ex.getMessage() == null) {
	// message.append(ex.getClass().getName());
	// }
	// if (ex.getMessage() != null) {
	// if (!baseIOException) {
	// message.append(" ");
	// }
	// message.append(ex.getMessage());
	// }
	// if (ex.getClass().getName().indexOf("MalformedInput") != -1) {
	// message.append(LINE_SEPARATOR);
	// message.append("This is normally due to the input file containing
	// invalid");
	// message.append(LINE_SEPARATOR);
	// message.append("bytes for the character encoding used : ");
	// message.append((inputEncoding == null ? fileUtils.getDefaultEncoding() :
	// inputEncoding));
	// message.append(LINE_SEPARATOR);
	// }
	// return message.toString();
	// }

	// ===========================================================================================================

	// /**
	// * Set whether or not you want the process to be spawned. Default is
	// false.
	// *
	// * @param spawn
	// * if true you do not want Ant to wait for the end of the
	// * process.
	// * @since Ant 1.6
	// */
	// public void setSpawn(boolean spawn) {
	// this.spawn = spawn;
	// }

	/**
	 * Set the timeout in milliseconds after which the process will be killed.
	 *
	 * @param value
	 *            timeout in milliseconds.
	 *
	 * @since Ant 1.5
	 */
	public void setTimeout(Long value) {
		timeout = value;
		incompatibleWithSpawn |= timeout != null;
	}

	/**
	 * Set the timeout in milliseconds after which the process will be killed.
	 *
	 * @param value
	 *            timeout in milliseconds.
	 */
	public void setTimeout(Integer value) {
		setTimeout((Long) ((value == null) ? null : new Long(value.intValue())));
	}

	/**
	 * Set the name of the forms compiler.
	 * 
	 * @param value
	 *            the name of the forms compiler.
	 */
	public void setFormsCompiler(String value) {
		this.formsCompiler = value;
	}

	/**
	 * Set the name of the reports compiler.
	 * 
	 * @param value
	 *            the name of the reports compiler.
	 */
	public void setReportsCompiler(String value) {
		this.reportsCompiler = value;
	}

	/**
	 * Set the working directory of the process.
	 * 
	 * @param d
	 *            the working directory of the process.
	 */
	public void setDir(File d) {
		this.dir = d;
	}

	/**
	 * List of operating systems on which the command may be executed.
	 * 
	 * @param os
	 *            list of operating systems on which the command may be
	 *            executed.
	 */
	public void setOs(String os) {
		this.os = os;
	}

	/**
	 * List of operating systems on which the command may be executed.
	 * 
	 * @since Ant 1.8.0
	 */
	public final String getOs() {
		return os;
	}

	/**
	 * File the output of the process is redirected to. If error is not
	 * redirected, it too will appear in the output.
	 *
	 * @param out
	 *            name of a file to which output should be sent.
	 */
	public void setOutput(File out) {
		this.output = out;
		incompatibleWithSpawn = true;
	}

	// /**
	// * Set the input file to use for the task.
	// *
	// * @param input
	// * name of a file from which to get input.
	// */
	// public void setInput(File input) {
	// if (inputString != null) {
	// throw new BuildException("The \"input\" and \"inputstring\" " +
	// "attributes cannot both be specified");
	// }
	// this.input = input;
	// incompatibleWithSpawn = true;
	// }

	// /**
	// * Set the string to use as input.
	// *
	// * @param inputString
	// * the string which is used as the input source.
	// */
	// public void setInputString(String inputString) {
	// if (input != null) {
	// throw new BuildException("The \"input\" and \"inputstring\" " +
	// "attributes cannot both be specified");
	// }
	// this.inputString = inputString;
	// incompatibleWithSpawn = true;
	// }

	/**
	 * Controls whether error output of exec is logged. This is only useful when
	 * output is being redirected and error output is desired in the Ant log.
	 *
	 * @param logError
	 *            set to true to log error output in the normal ant log.
	 */
	public void setLogError(boolean logError) {
		redirector.setLogError(logError);
		incompatibleWithSpawn |= logError;
	}

	/**
	 * Set the File to which the error stream of the process should be
	 * redirected.
	 *
	 * @param error
	 *            a file to which stderr should be sent.
	 *
	 * @since Ant 1.6
	 */
	public void setError(File error) {
		this.error = error;
		incompatibleWithSpawn = true;
	}

	/**
	 * Sets the property name whose value should be set to the output of the
	 * process.
	 *
	 * @param outputProp
	 *            name of property.
	 */
	public void setOutputproperty(String outputProp) {
		redirector.setOutputProperty(outputProp);
		incompatibleWithSpawn = true;
	}

	/**
	 * Sets the name of the property whose value should be set to the error of
	 * the process.
	 *
	 * @param errorProperty
	 *            name of property.
	 *
	 * @since Ant 1.6
	 */
	public void setErrorProperty(String errorProperty) {
		redirector.setErrorProperty(errorProperty);
		incompatibleWithSpawn = true;
	}

	/**
	 * Fail if the command exits with a non-zero return code.
	 *
	 * @param fail
	 *            if true fail the command on non-zero return code.
	 */
	public void setFailonerror(boolean fail) {
		failonerror = fail;
		incompatibleWithSpawn |= fail;
	}

	/**
	 * If the compile fails exit immediatly or exit after processing all
	 * filesets.
	 *
	 * @param fail
	 *            if true fail on the first compile error.
	 */
	public void setFailimmediate(boolean fail) {
		failimmediate = fail;
	}

	/**
	 * Do not propagate old environment when new environment variables are
	 * specified.
	 *
	 * @param newenv
	 *            if true, do not propagate old environment when new environment
	 *            variables are specified.
	 */
	public void setNewenvironment(boolean newenv) {
		newEnvironment = newenv;
	}

	/**
	 * Set whether to attempt to resolve the executable to a file.
	 *
	 * @param resolveExecutable
	 *            if true, attempt to resolve the path of the executable.
	 */
	public void setResolveExecutable(boolean resolveExecutable) {
		this.resolveExecutable = resolveExecutable;
	}

	/**
	 * Set whether to search nested, then system PATH environment variables for
	 * the executable.
	 *
	 * @param searchPath
	 *            if true, search PATHs.
	 */
	public void setSearchPath(boolean searchPath) {
		this.searchPath = searchPath;
	}

	/**
	 * Indicates whether to attempt to resolve the executable to a file.
	 * 
	 * @return the resolveExecutable flag
	 *
	 * @since Ant 1.6
	 */
	public boolean getResolveExecutable() {
		return resolveExecutable;
	}

	/**
	 * Add an environment variable to the launched process.
	 *
	 * @param var
	 *            new environment variable.
	 */
	public void addEnv(Environment.Variable var) {
		env.addVariable(var);
	}

	/**
	 * Sets the name of a property in which the return code of the command
	 * should be stored. Only of interest if failonerror=false.
	 *
	 * @since Ant 1.5
	 *
	 * @param resultProperty
	 *            name of property.
	 */
	public void setResultProperty(String resultProperty) {
		this.resultProperty = resultProperty;
		incompatibleWithSpawn = true;
	}

	/**
	 * Helper method to set result property to the passed in value if
	 * appropriate.
	 *
	 * @param result
	 *            value desired for the result property value.
	 */
	protected void maybeSetResultPropertyValue(int result) {
		if (resultProperty != null) {
			String res = Integer.toString(result);
			getProject().setNewProperty(resultProperty, res);
		}
	}

	/**
	 * Set whether to stop the build if program cannot be started. Defaults to
	 * true.
	 *
	 * @param flag
	 *            stop the build if program cannot be started.
	 *
	 * @since Ant 1.5
	 */
	public void setFailIfExecutionFails(boolean flag) {
		failIfExecFails = flag;
		incompatibleWithSpawn |= flag;
	}

	/**
	 * Set whether output should be appended to or overwrite an existing file.
	 * Defaults to false.
	 *
	 * @param append
	 *            if true append is desired.
	 *
	 * @since 1.30, Ant 1.5
	 */
	public void setAppend(boolean append) {
		redirector.setAppend(append);
		incompatibleWithSpawn |= append;
	}

	/**
	 * Add a <code>RedirectorElement</code> to this task.
	 *
	 * @param redirectorElement
	 *            <code>RedirectorElement</code>.
	 * @since Ant 1.6.2
	 */
	public void addConfiguredRedirector(RedirectorElement redirectorElement) {
		if (this.redirectorElement != null) {
			throw new BuildException("cannot have > 1 nested <redirector>s");
		}
		this.redirectorElement = redirectorElement;
		incompatibleWithSpawn = true;
	}

	/**
	 * Restrict this execution to a single OS Family
	 * 
	 * @param osFamily
	 *            the family to restrict to.
	 */
	public void setOsFamily(String osFamily) {
		this.osFamily = osFamily.toLowerCase(Locale.ENGLISH);
	}

	/**
	 * Restrict this execution to a single OS Family
	 * 
	 * @since Ant 1.8.0
	 */
	public final String getOsFamily() {
		return osFamily;
	}

	/**
	 * The method attempts to figure out where the executable is so that we can
	 * feed the full path. We first try basedir, then the exec dir, and then
	 * fallback to the straight executable name (i.e. on the path).
	 *
	 * @param exec
	 *            the name of the executable.
	 * @param mustSearchPath
	 *            if true, the executable will be looked up in the PATH
	 *            environment and the absolute path is returned.
	 *
	 * @return the executable as a full path if it can be determined.
	 *
	 * @since Ant 1.6
	 */
	protected String resolveExecutable(String exec, boolean mustSearchPath) {
		if (!resolveExecutable) {
			return exec;
		}
		// try to find the executable
		File executableFile = getProject().resolveFile(exec);
		if (executableFile.exists()) {
			return executableFile.getAbsolutePath();
		}
		// now try to resolve against the dir if given
		if (dir != null) {
			executableFile = FILE_UTILS.resolveFile(dir, exec);
			if (executableFile.exists()) {
				return executableFile.getAbsolutePath();
			}
		}
		// couldn't find it - must be on path
		if (mustSearchPath) {
			Path p = null;
			String[] environment = env.getVariables();
			if (environment != null) {
				for (int i = 0; i < environment.length; i++) {
					if (isPath(environment[i])) {
						p = new Path(getProject(), getPath(environment[i]));
						break;
					}
				}
			}
			if (p == null) {
				String path = getPath(Execute.getEnvironmentVariables());
				if (path != null) {
					p = new Path(getProject(), path);
				}
			}
			if (p != null) {
				String[] dirs = p.list();
				for (int i = 0; i < dirs.length; i++) {
					executableFile = FILE_UTILS.resolveFile(new File(dirs[i]), exec);
					if (executableFile.exists()) {
						return executableFile.getAbsolutePath();
					}
				}
			}
		}
		// mustSearchPath is false, or no PATH or not found - keep our
		// fingers crossed.
		return exec;
	}

	/**
	 * Execute a forms or reports compiler depending on file ending.
	 *
	 * @throws BuildException
	 *             in a number of circumstances:
	 *             <ul>
	 *             <li>if failIfExecFails is set to true and the process cannot
	 *             be started</li>
	 *             <li>the java13command launcher can send build exceptions</li>
	 *             <li>this list is not exhaustive or limitative</li>
	 *             </ul>
	 */
	public void executeCompiler(File srcFile, File destFile) throws BuildException {
		// Quick fail if this is not a valid OS for the command
		if (!isValidOs()) {
			return;
		}
		File savedDir = dir; // possibly altered in prepareExec

		formsCompilerCmdline.clearArgs();
		reportsCompilerCmdline.clearArgs();
		ArrayList<String> line = new ArrayList<String>();
		if (srcFile.getName().toLowerCase().endsWith(".rdf")) {
			line.add("source=" + srcFile.getAbsoluteFile());
			line.add("SType=rdffile");
			line.add("DType=repfile");
			line.add("batch=yes");
			line.add("overwrite=yes");
			line.add("Compile_All=YES");
			line.add("Userid=" + dbuserid);
			String[] lineArray = new String[line.size()];
			reportsCompilerCmdline.addArguments(line.toArray(lineArray));
			try {
				runExec(prepareExec(), srcFile, destFile, reportsCompilerCmdline);
			} finally {
				dir = savedDir;
			}
		} else {
			line.add("Module=" + srcFile.getAbsoluteFile());
			if (srcFile.getName().toLowerCase().endsWith(".pll")) {
				line.add("Module_Type=LIBRARY");
			} else if (srcFile.getName().toLowerCase().endsWith(".mmb")) {
				line.add("Module_Type=MENU");
			} else if (srcFile.getName().toLowerCase().endsWith(".fmb")) {
				line.add("Module_Type=FORM");
			}
			line.add("Logon=YES");
			line.add("Compile_ALL=SPECIAL");
			line.add("Userid=" + dbuserid);
			line.add("Batch=YES");
			line.add("Window_State=Minimize");
			String[] lineArray = new String[line.size()];
			formsCompilerCmdline.addArguments(line.toArray(lineArray));
			try {
				runExec(prepareExec(), srcFile, destFile, formsCompilerCmdline);
			} finally {
				dir = savedDir;
			}
		}

	}

	/**
	 * Has the user set all necessary attributes?
	 * 
	 * @throws BuildException
	 *             if there are missing required parameters.
	 */
	protected void checkConfiguration() throws BuildException {
		formsCompilerCmdline.setExecutable(resolveExecutable(formsCompiler, searchPath));
		reportsCompilerCmdline.setExecutable(resolveExecutable(reportsCompiler, searchPath));
		if (formsCompilerCmdline.getExecutable() == null) {
			throw new BuildException("no valid forms compiler specified", getLocation());
		}
		if (reportsCompilerCmdline.getExecutable() == null) {
			throw new BuildException("no valid reports compiler specified", getLocation());
		}
		if (dir != null && !dir.exists()) {
			throw new BuildException("The directory " + dir + " does not exist");
		}
		if (dir != null && !dir.isDirectory()) {
			throw new BuildException(dir + " is not a directory");
		}
		if (spawn && incompatibleWithSpawn) {
			getProject().log("spawn does not allow attributes related to input, " + "output, error, result",
					Project.MSG_ERR);
			getProject().log("spawn also does not allow timeout", Project.MSG_ERR);
			getProject().log("finally, spawn is not compatible " + "with a nested I/O <redirector>", Project.MSG_ERR);
			throw new BuildException(
					"You have used an attribute " + "or nested element which is not compatible with spawn");
		}
		setupRedirector();
	}

	/**
	 * Set up properties on the redirector that we needed to store locally.
	 */
	protected void setupRedirector() {
		redirector.setInput(input);
		redirector.setInputString(inputString);
		redirector.setOutput(output);
		redirector.setError(error);
	}

	/**
	 * Is this the OS the user wanted?
	 * 
	 * @return boolean.
	 *         <ul>
	 *         <li>
	 *         <li><code>true</code> if the os and osfamily attributes are null.
	 *         </li>
	 *         <li><code>true</code> if osfamily is set, and the os family and
	 *         must match that of the current OS, according to the logic of
	 *         {@link Os#isOs(String, String, String, String)}, and the result
	 *         of the <code>os</code> attribute must also evaluate true.</li>
	 *         <li><code>true</code> if os is set, and the system.property
	 *         os.name is found in the os attribute,</li>
	 *         <li><code>false</code> otherwise.</li>
	 *         </ul>
	 */
	protected boolean isValidOs() {
		// hand osfamily off to Os class, if set
		if (osFamily != null && !Os.isFamily(osFamily)) {
			return false;
		}
		// the Exec OS check is different from Os.isOs(), which
		// probes for a specific OS. Instead it searches the os field
		// for the current os.name
		String myos = System.getProperty("os.name");
		log("Current OS is " + myos, Project.MSG_VERBOSE);
		if ((os != null) && (os.indexOf(myos) < 0)) {
			// this command will be executed only on the specified OS
			log("This OS, " + myos + " was not found in the specified list of valid OSes: " + os, Project.MSG_VERBOSE);
			return false;
		}
		return true;
	}

	/**
	 * Set whether to launch new process with VM, otherwise use the OS's shell.
	 * Default value is true.
	 * 
	 * @param vmLauncher
	 *            true if we want to launch new process with VM, false if we
	 *            want to use the OS's shell.
	 */
	public void setVMLauncher(boolean vmLauncher) {
		this.vmLauncher = vmLauncher;
	}

	/**
	 * Create an Execute instance with the correct working directory set.
	 *
	 * @return an instance of the Execute class.
	 *
	 * @throws BuildException
	 *             under unknown circumstances.
	 */
	protected Execute prepareExec() throws BuildException {
		// default directory to the project's base directory
		if (dir == null) {
			dir = getProject().getBaseDir();
		}
		if (redirectorElement != null) {
			redirectorElement.configure(redirector);
		}
		Execute exe = new Execute(createHandler(), createWatchdog());
		exe.setAntRun(getProject());
		exe.setWorkingDirectory(dir);
		exe.setVMLauncher(vmLauncher);
		String[] environment = env.getVariables();
		if (environment != null) {
			for (int i = 0; i < environment.length; i++) {
				log("Setting environment variable: " + environment[i], Project.MSG_VERBOSE);
			}
		}
		exe.setNewenvironment(newEnvironment);
		exe.setEnvironment(environment);
		return exe;
	}

	/**
	 * A Utility method for this classes and subclasses to run an Execute
	 * instance (an external command).
	 *
	 * @param exe
	 *            instance of the execute class.
	 *
	 * @throws IOException
	 *             in case of problem to attach to the stdin/stdout/stderr
	 *             streams of the process.
	 */
	protected final void runExecute(Execute exe, File srcFile, File destFile) throws IOException {
		int returnCode = -1; // assume the worst

		if (!spawn) {
			// generate error file name
			String strippedFileName = null;
			File errorFile = null;
			// prevent file names starting with a dot to be modified
			if (srcFile.getName().lastIndexOf(".") > 0) {
				strippedFileName = srcFile.getName().substring(0, srcFile.getName().lastIndexOf("."));
				errorFile = new File(srcFile.getParentFile(), strippedFileName + ".err");
			}
			// delete old errorFile
			if (errorFile.exists()) {
				errorFile.delete();
			}
			returnCode = exe.execute();

			// test for and handle a forced process death
			if (exe.killedProcess()) {
				String msg = "Timeout: killed the sub-process";
				if (failonerror) {
					throw new BuildException(msg);
				} else {
					log(msg, Project.MSG_WARN);
				}
			}
			maybeSetResultPropertyValue(returnCode);
			redirector.complete();
			if (Execute.isFailure(returnCode)) {
				if (errorFile.canRead()) {
					logFile(errorFile, Project.MSG_ERR);
				}
				log("Result: " + returnCode, Project.MSG_ERR);
				if (destFile.exists()) {
					destFile.delete();
					log("deleted error prune result file: " + destFile.getAbsoluteFile(), Project.MSG_ERR);
				}
				if (failonerror) {
					if (failimmediate) {
						throw new BuildException(getTaskType() + " returned: " + returnCode, getLocation());
					} else {
						compiledWithError = true;
						filesWithError.add(srcFile.getCanonicalFile().toString());
					}
				}

			} else {
				// errorlevel was ok but sometimes a destfile is not created
				// (mostly seen with menu files)
				if (!destFile.exists() | (srcFile.lastModified() - granularity > destFile.lastModified())) {
					log(getTaskType() + " returned: " + returnCode + " but " + destFile.getAbsoluteFile()
							+ " is not created!", Project.MSG_ERR);
					if (errorFile.canRead()) {
						logFile(errorFile, Project.MSG_ERR);
					}
					// if this is an overwrite, delete the old existing dest
					// file
					if (destFile.exists()) {
						log("old existing dest file " + destFile.getAbsoluteFile() + " deleted!");
						destFile.delete();
					}
					if (failonerror) {
						if (failimmediate) {
							throw new BuildException(getTaskType() + " returned: " + returnCode + " but "
									+ destFile.getAbsoluteFile() + " is not created!", getLocation());
						} else {
							compiledWithError = true;
							filesWithError.add(srcFile.getCanonicalFile().toString());
						}
					}
				} else {
					if (errorFile.canRead()) {
						logFile(errorFile, verbosity);
					}
				}
			}
		} else {
			exe.spawn();
		}
	}

	/**
	 * Run the command using the given Execute instance. This may be overridden
	 * by subclasses.
	 *
	 * @param exe
	 *            instance of Execute to run.
	 *
	 * @throws BuildException
	 *             if the new process could not be started only if
	 *             failIfExecFails is set to true (the default).
	 */
	protected void runExec(Execute exe, File srcFile, File destFile, Commandline commandLine) throws BuildException {
		// show the command
		log(commandLine.describeCommand(), Project.MSG_VERBOSE);

		exe.setCommandline(commandLine.getCommandline());
		try {
			runExecute(exe, srcFile, destFile);
		} catch (IOException e) {
			if (failIfExecFails) {
				throw new BuildException("Execute failed: " + e.toString(), e, getLocation());
			} else {
				log("Execute failed: " + e.toString(), Project.MSG_ERR);
			}
		} finally {
			// close the output file if required
			logFlush();
		}
	}

	/**
	 * Create the StreamHandler to use with our Execute instance.
	 *
	 * @return instance of ExecuteStreamHandler.
	 *
	 * @throws BuildException
	 *             under unknown circumstances.
	 */
	protected ExecuteStreamHandler createHandler() throws BuildException {
		return redirector.createHandler();
	}

	/**
	 * Create the Watchdog to kill a runaway process.
	 *
	 * @return instance of ExecuteWatchdog.
	 *
	 * @throws BuildException
	 *             under unknown circumstances.
	 */
	protected ExecuteWatchdog createWatchdog() throws BuildException {
		return (timeout == null) ? null : new ExecuteWatchdog(timeout.longValue());
	}

	/**
	 * Flush the output stream - if there is one.
	 */
	protected void logFlush() {
	}

	private boolean isPath(String line) {
		return line.startsWith("PATH=") || line.startsWith("Path=");
	}

	private String getPath(String line) {
		return line.substring("PATH=".length());
	}

	private String getPath(Map<String, String> map) {
		String p = map.get("PATH");
		return p != null ? p : map.get("Path");
	}

	private void logFile(File file, int verbosity) {
		BufferedReader buffReader = null;
		try {
			buffReader = new BufferedReader(new FileReader(file));
			String line = buffReader.readLine();
			while (line != null) {
				log(file.getName() + ": " + line, verbosity);
				line = buffReader.readLine();
			}
			log("", verbosity);
		} catch (IOException ioe) {
			log("Error reading " + file.getAbsolutePath() + " " + ioe.getLocalizedMessage(), Project.MSG_ERR);
		} finally {
			try {
				buffReader.close();
			} catch (IOException ioe1) {
				// Leave It
			}
		}
	}

}
