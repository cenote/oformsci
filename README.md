
OFormsCI - Continuous Integration for Oracle Forms
---------------------------------------------------

OFormsCI is a set of tools to form a complete tool chain for continuous
integration with Git, Jenkins, Oracle Forms/Reports and WebLogic server.

It is partly based on scripts developed at [Cenote GmbH][] starting in 2003.

The official homepage is [oformsci.cenote.de][].

It has the following features:

  * Call a Forms/Reports Compiler and run it on an Ant fileset using a custom Ant Task
  * Compiles only changed files (if wanted) - speeds up intermediate builds
  * Fallback to full build if *.pll file changes detected
  * Save development workflow when switching branches
  * Circumvents compiler bugs which are hard to handle with shell scripts or windows batch files
  * Creates a jar file for icons and signs it (if you have a code signing key)
  * Works on Windows and Linux 
  * Comes with fully working example build.xml file - no Ant hacking
  * All customizing only via build.properties and project.propterties files


Requirements:

  * Java 1.6 or higher
  * Apache Ant (or Maven - not tested jet)
  * Git
  * Oracle Fusion Middleware Forms 11gR2 (maybe working with other versions too)

Optional:

  * Jenkins CI
  
  
### Quick Start

  * Download the OFormsCI from [downloads][].
  * Create a project directory with a maven like structure.
  * your Oracle Forms and Reports files should go into src/main/oforms/
  * Extract the distribution archive into your project directory.
  * Copy the files in oformsci/examples/* to your project basedir.
  * Rename build.properties.example to build.properties and tweak it according your installation.


### Release Notes

See [Changes] for a history of changes.


#### Known Bugs

For upcoming issues see [Issues][]


### Feedback

Feedback is always welcome! If you have any questions or proposals, don't
hesitate to write to our [discussion][] forum.

If you found a bug or you are missing a feature, log into our [Issuetracker][]
and create a bug or feature request.

If you like the software you can write a [review][] :-)


### Developement

The sourcecode is available at [bitbucket.org/cenote/oformsci][], the
project is hosted at [Sourceforge][].

OFormsCI is build with [Maven][]. 


To get a distribution package run:

    $ mvn package -P release


### License

Copyright 2015 Cenote GmbH.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Cenote GmbH]:http://www.cenote.de
[oformsci.cenote.de]:http://oformsci.cenote.de/
[Maven]:http://maven.apache.org/
[Sourceforge]:http://sourceforge.net/projects/oformsci/
[bitbucket.org/cenote/oformsci]:http://bitbucket.org/cenote/oformsci
[review]:http://sourceforge.net/projects/oformsci/reviews/
[discussion]:http://sourceforge.net/p/oformsci/discussion/
[Issues]:http://cenote-issues.atlassian.net/browse/OFC
[Issuetracker]:http://cenote-issues.atlassian.net/browse/OFC
[downloads]:downloads.html
[Changes]:changes.html

